package com.designpark;

import javax.lang.model.type.NullType;
import java.util.InputMismatchException;
import java.util.Scanner;

class Fahrkartenautomat
{
        static Scanner tastatur = new Scanner(System.in);

        //Fahrkarten Bestellung
        public static double fahrkartenbestellungErfassen() {
            double zuZahlenderBetrag;
            int Ticket;
            int anzahlFahrkarten;

           //Scanner tastatur = new Scanner(System.in);

            System.out.print("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus: ");
            System.out.println("\n Einzelfahrschein Regeltarif AB[2,90 EUR] (1)");
            System.out.println(" Tageskarte Regeltarif AB[8,60 EUR] (2)");
            System.out.println(" Kleingruppen-Tageskarte Regeltarif AB[23,50 EUR] (3)");
            Ticket =tastatur.nextInt();
            zuZahlenderBetrag=0;

            switch(Ticket){

                case 1:
                    System.out.println("Ihre Wahl:" + Ticket);
                    zuZahlenderBetrag = 2.90 ;
                    break;

                case 2:
                    System.out.println("Ihre Wahl:" + Ticket);
                    zuZahlenderBetrag = 8.60 ;
                    break;

                case 3:
                    System.out.println("Ihre Wahl:" + Ticket);
                    zuZahlenderBetrag = 23.50 ;
                    break;
                default:
                    while (true)
                    {
                        System.out.println(">>falsche eingabe<<");
                        Ticket=tastatur.nextInt();
                        switch(Ticket) {

                            case 1:
                                System.out.println("Ihre Wahl:" + Ticket);
                                return zuZahlenderBetrag = 2.90;

                            case 2:
                                System.out.println("Ihre Wahl:" + Ticket);
                                return zuZahlenderBetrag = 8.60;


                            case 3:
                                System.out.println("Ihre Wahl:" + Ticket);
                                return zuZahlenderBetrag = 23.50;
                        }

                    }

            }



            //System.out.print("Zu zahlender Betrag (EURO): ");
            //zuZahlenderBetrag = tastatur.nextDouble();


            System.out.println("Bitte die Anzahl der gewünschten Fahrscheine eingeben:");
            anzahlFahrkarten = getTicketInt();


            while (anzahlFahrkarten < 0 || anzahlFahrkarten > 10 )
            {
                System.out.println("Bitte geben Sie eine positive Zahl zwischen 1 und 10 ein:");
                anzahlFahrkarten = getTicketInt();
            }

            zuZahlenderBetrag = zuZahlenderBetrag * anzahlFahrkarten;
            return zuZahlenderBetrag;
        }

        //Fängt eingaben von != int ab
        private static int getTicketInt()
        {
            while (true){
                try {
                    return tastatur.nextInt();
                }catch (InputMismatchException e){
                    tastatur.next();
                    System.out.println("Bitte geben Sie eine positive Zahl zwischen 1 und 10 ein:");
                }
            }
        }


      /*  public static double getPrice()
        {
            double zuZahlenderBetrag;

            while (true)
            {
                if (wunschTicket() == 1) {
                    zuZahlenderBetrag = 2.90;
                } else if (wunschTicket() == 2) {
                    zuZahlenderBetrag = 8.60;
                } else if (wunschTicket() == 3) {
                    zuZahlenderBetrag = 23.50;
                }
                return zuZahlenderBetrag;
            }
        }*/










        // Fahrkarten Bezahlung
        public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
            double eingezahlterGesamtbetrag;
            double eingeworfeneMünze;
            double rückgabebetrag;
            Scanner tastatur = new Scanner(System.in);
            eingezahlterGesamtbetrag = 0.0;
            while (eingezahlterGesamtbetrag < zuZahlenderBetrag)
            {
                double zwischenergebnis = zuZahlenderBetrag - eingezahlterGesamtbetrag;
                System.out.printf("Noch zu zahlen: %.2f Euro\n", zwischenergebnis);
                System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
                eingeworfeneMünze = tastatur.nextDouble();
                eingezahlterGesamtbetrag += eingeworfeneMünze;
            }
            fahrkartenAusgeben();

            rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
            return rückgabebetrag;
        }


        // Fahrscheinausgabe
        public static void fahrkartenAusgeben() {
            System.out.println("\nFahrschein wird ausgegeben");
            for (int i = 0; i < 8; i++) {
                System.out.print("=");
                try {
                    Thread.sleep(250);
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            System.out.println("\n\n");
        }


            // Rückgeldberechnung und -Ausgabe
            public static void rueckgeldAusgeben(double rückgabebetrag){

                if (rückgabebetrag > 0.00) {
                    System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
                    System.out.println("wird in folgenden Münzen ausgezahlt:");

                    while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
                    {
                        System.out.println("2 EURO");
                        rückgabebetrag -= 2.0;
                    }
                    while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
                    {
                        System.out.println("1 EURO");
                        rückgabebetrag -= 1.0;
                    }
                    while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
                    {
                        System.out.println("50 CENT");
                        rückgabebetrag -= 0.5;
                    }
                    while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
                    {
                        System.out.println("20 CENT");
                        rückgabebetrag -= 0.2;
                    }
                    while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
                    {
                        System.out.println("10 CENT");
                        rückgabebetrag -= 0.1;
                    }
                    while (rückgabebetrag >= 0.05)// 5 CENT-Münzen
                    {
                        System.out.println("5 CENT");
                        rückgabebetrag -= 0.05;
                    }
                    while (rückgabebetrag >= 0.01) {
                        System.out.println("1 Cent");
                        rückgabebetrag -= 0.01;
                    }
                }
                System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                        "vor Fahrtantritt entwerten zu lassen!\n"+
                        "Wir wünschen Ihnen eine gute Fahrt.");
            }



                public static void main(String[] args)
                {
                    double zuZahlenderBetrag;
                    double rückgabebetrag;
                    while (true)
                    {
                    zuZahlenderBetrag = fahrkartenbestellungErfassen();
                    rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
                    rueckgeldAusgeben(rückgabebetrag);
                    System.out.println("\n");
                    }
                }


    }